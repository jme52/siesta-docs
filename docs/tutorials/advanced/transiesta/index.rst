:sequential_nav: next

..  _tutorial-transiesta:

TranSIESTA introduction
------------------------

The TranSIESTA tutorial is ran on a jupyter notebook. It processes with python the results of SIESTA simulations.

The notebook can be downloaded by clicking :download:`here <work-files/C_chain/TranSIESTA_tutorial.ipynb>`

If you are not sure whether you have all the dependencies to run the tutorial, run it on `google colab <https://colab.research.google.com>`_ , it works!