gnuplot -persist <<-EOFMarker
et term png
set output "preassure.png"
set xlabel 'timestep'
set ylabel 'Preassure (kbar)'
p 'si8.MDE' u 1:6 w l t 'P'
EOFMarker
