gnuplot -persist <<-EOFMarker
set term png
set output "Eks_total.png"
set xlabel 'timestep'
set ylabel 'Energy (eV)'
p 'si8.MDE' u 1:3 w l t 'E_{KS}', 'si8.MDE' u 1:4 w l t 'E_{tot}'
EOFMarker
