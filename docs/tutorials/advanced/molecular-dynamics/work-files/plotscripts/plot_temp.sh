gnuplot -persist <<-EOFMarker
set term png
set output "temperature.png"
set xlabel 'timestep'
set ylabel 'Temperature (K)'
p 'si8.MDE' u 1:2 w l t 'T'
EOFMarker
