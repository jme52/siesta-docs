.. _introduction:


Tutorials
=========

This set of tutorials will guide you in the exploration of Siesta's features.

**Before you do anything else, start here.** You need to set up your local
working environment to follow the tutorial.

..  toctree::
    :maxdepth: 1

    00-TutorialSetup.rst


Basics of Siesta
----------------

This section is recommended for all beginners, and also as a refresher
for more experienced users.

..  toctree::
    :maxdepth: 1

    basic/first-encounter/index
    basic/first-encounter-theorylevel/index
    basic/basis-optimization/index
    basic/basis-sets/index
    basic/grid-convergence/index
    basic/kpoint-convergence/index
    basic/scf-convergence/index
    basic/analysis-tools/index
    basic/structure-optimization/index
    basic/vibrational-properties/index
    basic/magnetism/index
    basic/first-crystals/index


Intermediate and Advanced Topics
--------------------------------

This section provides deeper introductions to relevant topics.

..  toctree::
    :maxdepth: 1

    advanced/advanced-analysis/index.rst
    advanced/molecular-dynamics/index.rst
    advanced/phonons/index
    advanced/spin-orbit/index.rst
    advanced/polarization/index.rst
    advanced/dft+u/index.rst
    advanced/td-dft/index.rst
    advanced/wannier/index.rst
    advanced/neb/index.rst
    advanced/unfolding/index
    advanced/transiesta/index


Advanced applications
---------------------

..  toctree::
    :maxdepth: 1

    applications/optical-properties/index.rst
    applications/tb2j/index.rst
    applications/stm-images/index


SISL, Lua and scripting
-----------------------

..  toctree::
    :maxdepth: 1

    scripting/sisl/index
    scripting/lua-engine/index
    scripting/aiida-siesta/index.rst
