grep " constrained" OUT | awk '{print NR, $2}'> forces.data

gnuplot -persist <<-EOFMarker
set term png
set output "max_force_iteration.png"
set xlabel '# iteration'
set ylabel 'Force (eV/Ang)'
p 'forces.data' u 1:2 w l
EOFMarker
