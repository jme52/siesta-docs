Siesta Documentation
==============================

.. rst-class:: center
    
.. figure:: https://siesta-project.org/siesta/SIESTA-logo-233x125.png
    :alt: SIESTA logo

| |Original paper| |Latest paper| |Original TranSIESTA paper| |Latest TranSIESTA paper|
| |Install SIESTA using conda| |Conda Downloads|

**SIESTA** is a program for **efficient electronic structure
calculations** and ab initio molecular dynamics simulations of molecules
and solids in the framework of **Density-Functional Theory (DFT)**.

.. grid:: 1 1 2 2
    :gutter: 2

    .. grid-item-card:: :fas:`screwdriver-wrench` -- Installation.
        :link: installation/quick-install
        :link-type: doc

        Installing SIESTA is easier than you might think, check out our quick installation guide.

    .. grid-item-card::  :fas:`person-chalkboard` -- Tutorials
        :link: tutorials/index
        :link-type: doc

        Let us show you how to do things with SIESTA!

    .. grid-item-card::  :fas:`book-open` -- User guide
        :link: reference/siesta
        :link-type: doc

        Explore all the features of SIESTA.

    .. grid-item-card:: :fab:`discord` -- Chat with us
        :link: https://discord.gg/AqjX6aTNXR
        :link-type: url

        Join our Discord channel to share results or get help.

.. toctree::
    :maxdepth: 1
    :hidden:

    installation/index.rst
    tutorials/index.rst
    analysis_visualization/index.rst
    reference/index.rst
   

.. |Original paper| image:: https://img.shields.io/endpoint?url=https%3A%2F%2Fapi.juleskreuer.eu%2Fcitation-badge.php%3Fshield%26doi%3D10.1088%2F0953-8984%2F14%2F11%2F302&label=Original%20paper
   :target: https://doi.org/10.1088/0953-8984/14/11/302
.. |Latest paper| image:: https://img.shields.io/endpoint?url=https%3A%2F%2Fapi.juleskreuer.eu%2Fcitation-badge.php%3Fshield%26doi%3D10.1063%2F5.0005077&label=Latest%20paper
   :target: https://doi.org/10.1063/5.0005077
.. |Original TranSIESTA paper| image:: https://img.shields.io/endpoint?url=https%3A%2F%2Fapi.juleskreuer.eu%2Fcitation-badge.php%3Fshield%26doi%3D10.1103%2FPhysRevB.65.165401&label=Original%20TranSIESTA%20paper
   :target: https://doi.org/10.1103/PhysRevB.65.165401
.. |Latest TranSIESTA paper| image:: https://img.shields.io/endpoint?url=https%3A%2F%2Fapi.juleskreuer.eu%2Fcitation-badge.php%3Fshield%26doi%3D10.1016%2Fj.cpc.2016.09.022&label=Latest%20TranSIESTA%20paper
   :target: https://doi.org/10.1016/j.cpc.2016.09.022
.. |Install SIESTA using conda| image:: https://img.shields.io/conda/v/conda-forge/siesta
   :target: https://anaconda.org/conda-forge/siesta
.. |Conda Downloads| image:: https://anaconda.org/conda-forge/siesta/badges/downloads.svg
   :target: https://anaconda.org/conda-forge/siesta
