
TEX_ROOT=$(pwd)/$(dirname $0)
echo "Pandoc manual conversion. Using TEX_ROOT=$TEX_ROOT"

# The BSD and GNU sed commands inplace option works differently, 
# therefore we need to use a temporary file
SED_TEMP_FILE="${TEX_ROOT}/tmp_file"
sed_in_place() {
    # Get the last argument, which is the name of the file to modify
    for file in "$@"; do true; done

    # Use sed to perform the command and write to a temp file, then move it back
    sed "$@" > ${SED_TEMP_FILE} && mv ${SED_TEMP_FILE} "$file"
}

prepare_tex_file(){
    # Makes sure a tex file is ready for conversion.
    # First argument is the file to clean
    # Second argument is optional. If it is "helper", we will interpret that we are processing a helper file.
    
    # If second argument is true
    if [ "$2" = helper ]; then
        sed_in_place \
            -e 's/\\begin{fdfentry}{\(.*\)}\[\(.*\)\]<\(.*\)>/\\begin{fdfentry}{\1}{\2}{\3}/g' \
            -e 's/\\begin{fdfentry}{\(.*\)}\[\(.*\)\]/\\begin{fdfentry}{\1}{\2}{}/g' \
            -e 's/\\begin{fdfentry}{\(.*\)}<\(.*\)>/\\begin{fdfentry}{\1}{}{\2}/g' \
            -e 's/\\begin{fdfentry}{\(.*\)}/\\begin{fdfentry}{\1}{}{}/g' \
            -e 's/\\begin{fdfexample}/\\begin{verbatim}/g' \
            -e 's/\\end{fdfexample}/\\end{verbatim}/g' \
            -e 's/\\begin{codeexample}/\\begin{verbatim}/g' \
            -e 's/\\end{codeexample}/\\end{verbatim}/g' \
            -e 's/\\begin{shellexample}/\\begin{verbatim}/g' \
            -e 's/\\end{shellexample}/\\end{verbatim}/g' \
            -e 's/\\begin{output}/\\begin{verbatim}/g' \
            -e 's/\\end{output}/\\end{verbatim}/g' \
            $1
    else
        sed_in_place -e 's/\\begin{fdfentry}{\(.*\)}\[\(.*\)\]<\(.*\)>/\\begin{fdfentry}{\1}{\2}{\3}/g' \
            -e 's/\\begin{fdfentry}{\(.*\)}\[\(.*\)\]/\\begin{fdfentry}{\1}{\2}{}/g' \
            -e 's/\\begin{fdfentry}{\(.*\)}<\(.*\)>/\\begin{fdfentry}{\1}{}{\2}/g' \
            -e 's/\\begin{fdfentry}{\(.*\)}/\\begin{fdfentry}{\1}{}{}/g' \
            -e 's/\\begin{fdfexample}/\\begin{verbatim}/g' \
            -e 's/\\end{fdfexample}/\\end{verbatim}/g' \
            -e 's/\\begin{codeexample}/\\begin{verbatim}/g' \
            -e 's/\\end{codeexample}/\\end{verbatim}/g' \
            -e 's/\\begin{shellexample}/\\begin{verbatim}/g' \
            -e 's/\\end{shellexample}/\\end{verbatim}/g' \
            -e 's/\\begin{output}/\\begin{verbatim}/g' \
            -e 's/\\end{output}/\\end{verbatim}/g' \
            -e 's/\\fdfindex\*/\\fdfindexstar/g' \
            -e 's/\\fdf\*/\\fdfstar/g' \
            $1
    fi

}

# Function that replaces placeholders that have been used to avoid pandoc
# messing with special characters that we want to write literally.
postprocess_rst_specialchars(){
    sed_in_place \
        -e 's/PLACEHOLDERSIESTA_PANDOC_BACKSLASHPLACEHOLDER/\\/g' \
        -e 's/PLACEHOLDERSIESTA_PANDOC_BACKTICKPLACEHOLDER/`/g' \
        "$1"
}

# We will download the SIESTA tex manual from here
SIESTA_LINK="https://gitlab.com/siesta-project/siesta/-/archive/dev/siesta-dev.tar.gz?path=Docs/tex"
TBTRANS_LINK="https://gitlab.com/siesta-project/siesta/-/raw/dev/Docs/tbtrans.tex?ref_type=heads&inline=false"
BIB_LINK="https://gitlab.com/siesta-project/siesta/-/raw/dev/Docs/siesta.bib?ref_type=heads&inline=false"

rm -rf ${TEX_ROOT}/tex
# Download
echo "DOWNLOADING SIESTA MANUAL'S TEX FILES..."
curl ${SIESTA_LINK} --output - | (tar --directory "$TEX_ROOT" --strip-components=2 -xzf -)
curl ${TBTRANS_LINK} --output ${TEX_ROOT}/tex/tbtrans.tex
curl ${BIB_LINK} --output ${TEX_ROOT}/siesta.bib

echo "PREPROCESSING TEX FILES..."

# If we don't do this, there is a "siestatextsize" at the beggining of the file
# (because of pandoc's inability to parse complex tex files)
sed_in_place 's/ siestatextsize//' ${TEX_ROOT}/tex/helpers/setup.tex

# Cut the helper tex files if needed to avoid tex that pandoc can't parse
for f in ${TEX_ROOT}/tex/helpers/*; do
    python ${TEX_ROOT}/cut_pandoc_file.py $f
done

# Preprocess the tex files as needed by our redefined latex commands and pandoc's limitations.
for f in ${TEX_ROOT}/tex/helpers/*.tex; do
    prepare_tex_file "$f" helper
done

for f in ${TEX_ROOT}/tex/*.tex ${TEX_ROOT}/tex/sections/*.tex ${TEX_ROOT}/tex/sections/*/*.tex; do
    prepare_tex_file "$f" 
done

# Incorporate custom sphinx commands into tbtrans.tex.
# We don't need to do this for the siesta manual because we use a custom tex template.
sed_in_place '15s/^/\\input{.\/sphinx_commands.tex}\n/' ${TEX_ROOT}/tex/tbtrans.tex
# Remove the SIESTA version input from tbtrans.tex to avoid warnings.
sed_in_place 's/\\input{..\/SIESTA.version}//g' ${TEX_ROOT}/tex/tbtrans.tex

echo "CONVERTING TEX TO RST WITH PANDOC..."

# Run pandoc
# We need this cd because the \input{} commands in the tex files 
# are interpreted relative to the current directory.
cd ${TEX_ROOT}
pandoc ${TEX_ROOT}/rst_siesta_manual.tex --filter ${TEX_ROOT}/pandoc_filter.py --citeproc -o ${TEX_ROOT}/../siesta.rst
pandoc ${TEX_ROOT}/tex/tbtrans.tex --filter ${TEX_ROOT}/pandoc_filter.py --citeproc -o ${TEX_ROOT}/../tbtrans.rst

echo "POSTPROCESSING RST FILES..."
# Replace any placeholders that we have used to write special characters.
postprocess_rst_specialchars ${TEX_ROOT}/../siesta.rst
postprocess_rst_specialchars ${TEX_ROOT}/../tbtrans.rst

# Make sure that there is a top level title in the tbtrans manual
sed_in_place '1s/^/TBTrans User Guide\n******************\n/' ${TEX_ROOT}/../tbtrans.rst
