SIESTA Quick install
####################
.. note::
  If anything fails, see :doc:`build-issues`.


.. tab:: conda

  .. figure:: https://anaconda.org/conda-forge/siesta/badges/platforms.svg
    :target: https://anaconda.org/conda-forge/siesta
    :alt: Platforms



  **Serial installation:**

  .. code-block:: bash

    conda install -c conda-forge siesta

  **Parallel installation with OpenMPI:**

  .. code-block:: bash

    conda install -c conda-forge siesta=*=*openmpi*

  **Parallel installation with mpich:**

  .. code-block:: bash

    conda install -c conda-forge siesta=*=*openmpi*

  For more details, refer to :doc:`conda`.


.. tab:: Building from source

  **Requirements**

  These are the minimal requirements for your SIESTA installation:

  * Fortran and C compilers.
  * **CMake** >= 3.20
  * **BLAS and LAPACK** linear algebra libraries (or compatible implementation, such as IntelMKL).

  To achieve peak performance and make the most out of your machine,
  parallelization is also needed. Therefore, the following libraries are strongly
  recommended:

  * An **MPI** library (OpenMPI, IntelMPI, etc).
  * **ScaLAPACK** parallel linear algebra library (or compatible implementation).
  * Fortran-compatible **NetCDF**.

  If you have a modern Debian-based system (such as Ubuntu), you can easily install
  all of this via apt:

  .. code-block:: shell

    sudo apt install gfortran cmake libopenmpi-dev liblapack-dev libscalapack-openmpi-dev libnetcdff-dev

  If you will require PEXSI support, you will also need bison and flex:

  .. code-block:: shell

    sudo apt install g++ bison flex


  **Building SIESTA**
  (*See also* :doc:`build-manually`)

  The most basic compilation of Siesta can be done simply by:

  .. code-block:: shell

    cmake -S. -B_build -DCMAKE_INSTALL_PREFIX=/path/to/installation
    cmake --build _build -j 4
    cmake --install _build

  During the build stage, the "-j" option indicates the amount of processes to use for
  compilation. This will greatly speed up the whole process, but be mindful of the
  total amount of CPUs available in your machine.


.. tab:: spack

  (*See* :doc:`spack`)
